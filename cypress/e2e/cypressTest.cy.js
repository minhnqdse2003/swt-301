describe("Recording 9/21/2023 at 12:53:10 AM", () => {
  it("tests Recording 9/21/2023 at 12:53:10 AM", () => {
    cy.viewport(930, 1001);
    cy.visit("http://localhost:3000/");
    cy.get("#\\:r1\\:").click();
    cy.get("#\\:r1\\:").type("12");
    cy.get("#\\:r3\\:").click();
    cy.get("#\\:r3\\:").type("12");
    cy.get("#\\:r5\\:").click();
    cy.get("#\\:r5\\:").type("2023");
    cy.get("[data-test='check-button']").click();
    cy.get("[data-test='close-dialog-button']").click();
    cy.get("#\\:r1\\:").click();
    cy.get("#\\:r1\\:").type("12");
    cy.get("#\\:r3\\:").click();
    cy.get("#\\:r3\\:").type("12");
    cy.get("#\\:r5\\:").click();
    cy.get("#\\:r5\\:").type("12");
    cy.get("[data-test='clear-button']").click();
  });
});
