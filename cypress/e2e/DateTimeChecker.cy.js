describe("Style testing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 (Text)", () => {
    cy.getDataTest("title-label").should("include.text", "Date Time Checker");
  });
  it("Test 2 (Color)", () => {
    cy.getDataTest("title-label").should("have.css", "color", "rgb(0, 0, 255)");
  });
  it("Test 3 (Font)", () => {
    cy.getDataTest("title-label").should("have.css", "font-family", "Arial");
  });
});

describe("Clear button testing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 (Input then clear)", () => {
    cy.getDataTest("clear-button").as("clear");
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");

    //Input Field
    cy.get("@day").click().type("12");
    cy.get("@month").click().type("123");
    cy.get("@year").click().type("123");
    //Handle On Click Clear
    cy.get("@clear").click();
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Day out of range testing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( day<0 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("-12");
    cy.get("@month").type("123");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day. Please enter a value between 1 and 31./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( day=0 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("0");
    cy.get("@month").type("123");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day. Please enter a value between 1 and 31./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( day>31 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("32");
    cy.get("@month").type("123");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day. Please enter a value between 1 and 31./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Month out of range testing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( month>12 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("123");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid month. Please enter a value between 1 and 12./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( month<0 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("-12");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid month. Please enter a value between 1 and 12./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( month=0 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("0");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid month. Please enter a value between 1 and 12./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Year out of range testing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( year<0 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("12");
    cy.get("@year").type("-1233");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid year. Please enter a value between 1000 and 3000./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( year<1000 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("12");
    cy.get("@year").type("123");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid year. Please enter a value between 1000 and 3000./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( year>3000 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("12");
    cy.get("@month").type("12");
    cy.get("@year").type("3200");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid year. Please enter a value between 1000 and 3000./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Invalid day in a month", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( month=4 & day>30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("31");
    cy.get("@month").type("4");
    cy.get("@year").type("1997");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day for the selected month./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( month=6 & day>30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("31");
    cy.get("@month").type("6");
    cy.get("@year").type("1997");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day for the selected month./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( month=9 & day>30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("31");
    cy.get("@month").type("9");
    cy.get("@year").type("1997");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day for the selected month./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 4 ( month=11 & day>30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("31");
    cy.get("@month").type("11");
    cy.get("@year").type("1997");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(
      /Invalid day for the selected month./i
    );
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Invalid day for February ( Leap Year )", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( day>29 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("2");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Invalid day for February./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Invalid day for February ( Not A Leap Year )", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( day>28 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("29");
    cy.get("@month").type("2");
    cy.get("@year").type("2019");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Invalid day for February./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Valid day for February ( Leap Year && Not A Leap Year )", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( Leap year/Day=29 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("29");
    cy.get("@month").type("2");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( Not A Leap year/Day=28)", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("28");
    cy.get("@month").type("2");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Valid day in a month with Leap Year", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( month=4/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("4");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( month=6/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("6");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( month=9/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("9");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 4 ( month=11/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("11");
    cy.get("@year").type("2020");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});

describe("Valid day in a month without Leap Year", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1 ( month=4/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("4");
    cy.get("@year").type("2019");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 2 ( month=6/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("6");
    cy.get("@year").type("2019");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 3 ( month=9/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("9");
    cy.get("@year").type("2019");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });

  it("Test 4 ( month=11/day=30 )", () => {
    cy.getDataTest("day-field").as("day");
    cy.getDataTest("month-field").as("month");
    cy.getDataTest("year-field").as("year");
    cy.getDataTest("check-button").as("check");

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //Input
    cy.get("@day").type("30");
    cy.get("@month").type("11");
    cy.get("@year").type("2019");
    cy.get("@check").click();

    // Check if the dialog is popup or not
    cy.getDataTest("dialog-container").should("exist");
    cy.getDataTest("close-dialog-button").should("exist");
    cy.getDataTest("expected-result").should("exist");

    //Check the result
    cy.getDataTest("expected-result").contains(/Valid date and time./i);
    cy.getDataTest("close-dialog-button").click();

    //Then the popup should be disabled
    cy.getDataTest("dialog-container").should("not.exist");
    cy.getDataTest("close-dialog-button").should("not.exist");
    cy.getDataTest("expected-result").should("not.exist");

    //And the input field must be empty
    cy.get("@day").should("have.value", "");
    cy.get("@month").should("have.value", "");
    cy.get("@year").should("have.value", "");
  });
});
