import logo from './logo.svg';
import './App.css';
import DateTimeChecker from './components/DateTimeChecker';

function App() {
  return (
    <div className="App">
      <DateTimeChecker/>
    </div>
  );
}

export default App;
