import React from "react";
import "../css/DateTimeChecker.css";
import { useState } from "react";
import {
  TextField,
  Button,
  Grid,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";

function DateTimeChecker() {
  const [day, setDay] = useState("");
  const [month, setMonth] = useState("");
  const [year, setYear] = useState("");

  const [result, setResult] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const handleClear = () => {
    setDay("");
    setMonth("");
    setYear("");
  };

  const handleClose = () => {
    setIsOpen(false);
    setResult("");
  };

  const handleOnClickCheckButton = () => {
    const validDayRange = day >= 1 && day <= 31;
    const validMonthRange = month >= 1 && month <= 12;
    const validYearRange = year >= 1000 && year <= 3000;

    if (isNaN(day) || isNaN(month) || isNaN(year)) {
      setResult(
        "Invalid input. Please enter numeric values for day, month, and year."
      );
      handleClear();
      return;
    }

    if (!validDayRange) {
      setResult("Invalid day. Please enter a value between 1 and 31.");
      handleClear();
      return;
    }

    if (!validMonthRange) {
      setResult("Invalid month. Please enter a value between 1 and 12.");
      handleClear();
      return;
    }

    if (!validYearRange) {
      setResult("Invalid year. Please enter a value between 1000 and 3000.");
      handleClear();
      return;
    }

    if (month == 2) {
      // February
      const isLeapYear =
        (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
      const validFebruaryRange =
        (isLeapYear && day >= 1 && day <= 29) ||
        (!isLeapYear && day >= 1 && day <= 28);

      if (!validFebruaryRange) {
        setResult("Invalid day for February.");
        handleClear();
        return;
      }
    } else {
      const validDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      const validMonthDayRange = day >= 1 && day <= validDaysInMonth[month - 1];

      if (!validMonthDayRange) {
        setResult("Invalid day for the selected month.");
        handleClear();
        return;
      }
    }
    setResult("Valid date and time.");
    handleClear();
    return;
  };

  return (
    <Grid
      style={{ width: "50vw", margin: "0 auto" }}
      container
      rowSpacing={1}
      spacing={{ xs: 1, sm: 2, md: 3 }}
    >
      <Grid item xs={12}>
        <Typography
          data-test="title-label"
          variant="h5"
          style={{
            color: "blue",
            fontFamily: "Arial",
            fontSize: "26px",
          }}
        >
          Date Time Checker
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Day"
          value={day}
          onChange={(e) => setDay(e.target.value)}
          type="number"
          variant="outlined"
          data-test="day-field"
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Month"
          value={month}
          onChange={(e) => setMonth(e.target.value)}
          type="number"
          variant="outlined"
          data-test="month-field"
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          label="Year"
          value={year}
          onChange={(e) => setYear(e.target.value)}
          type="number"
          variant="outlined"
          data-test="year-field"
          fullWidth
        />
      </Grid>
      <Grid container rowSpacing={2} spacing={2}>
        <Grid item xs={6}>
          <Button
            data-test="check-button"
            variant="contained"
            color="primary"
            onClick={() => {
              handleOnClickCheckButton();
              console.log(result);
              setIsOpen(true);
            }}
          >
            Check
          </Button>
        </Grid>

        <Grid item xs={6}>
          <Button
            data-test="clear-button"
            variant="contained"
            color="primary"
            onClick={handleClear}
          >
            Clear
          </Button>
        </Grid>
      </Grid>
      <Dialog data-test="dialog-container" open={isOpen} onClose={handleClose}>
        <DialogTitle>Result</DialogTitle>
        <DialogContent>
          <Typography data-test="expected-result">{result}</Typography>
        </DialogContent>
        <DialogActions>
          <Button
            data-test="close-dialog-button"
            onClick={handleClose}
            color="primary"
          >
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </Grid>
  );
}

export default DateTimeChecker;
